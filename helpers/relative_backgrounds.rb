require 'pathname'

def relative_backgrounds
  images_dir =  File.expand_path(config.images_dir, source_dir)
  images_dir =  Pathname.new File.expand_path(data.config.backgrounds_folder, images_dir)
  current_dir = Pathname.new File.expand_path(".#{current_page.url}", source_dir)

  images_dir.relative_path_from current_dir
end