# https://github.com/middleman/middleman-blog/blob/master/lib/middleman-blog/blog_data.rb
module Middleman
  module Blog
    class BlogData

      # Same as `articles` but with ascending order
      def articles_asc
        @_articles.sort_by(&:date)
      end

    end
  end
end