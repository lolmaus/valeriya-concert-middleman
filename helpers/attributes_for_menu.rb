def attributes_for_menu_item(section, sections = nil)
  # We need all those fake attributes
  # for Skrollr to apply the `skrollable-between` classname
  {}.tap do |attrs|
    attrs[:class] = "_#{section.slug}"
    attrs['data-anchor-target'] = "##{section.slug}"

    if !sections or sections.index(section) != 0
      attrs["data-#{data.config.menu_transition_gap_half}-center-top"] = "opacity: 1;"
      attrs["data--#{data.config.menu_transition_gap_half}-center-top"] = "opacity: 1;"
    end

    if !sections or sections.index(section) != sections.length - 1
      attrs["data-#{data.config.menu_transition_gap_half}-center-bottom"] = "opacity: 1;"
      attrs["data--#{data.config.menu_transition_gap_half}-center-bottom"] = "opacity: 1;"
    end
  end
end

def attributes_for_menu_background(section, sections = nil)
  {}.tap do |attrs|
    attrs['data-anchor-target'] = "##{section.slug}"

    if !sections or sections.index(section) != 0
      attrs["data-#{data.config.menu_transition_gap_half}-center-top"] = "opacity: 0;"
      attrs["data--#{data.config.menu_transition_gap_half}-center-top"] = "opacity: 1;"
    end

    if !sections or sections.index(section) != sections.length - 1
      attrs["data-#{data.config.menu_transition_gap_half}-center-bottom"] = "opacity: 1;"
      attrs["data--#{data.config.menu_transition_gap_half}-center-bottom"] = "opacity: 0;"
    end
  end
end