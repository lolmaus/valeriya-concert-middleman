# Retrieving a list of menu items
window.$menu_items = $ '.menu-item'
window.$menu_button_prev = $ '.menu-button._prev'
window.$menu_button_next = $ '.menu-button._next'
window.$window = $ window


current_classname = '-current'
disabled_classname = '-disabled'

current_menu_item = (curTop = skrollr_instance.getScrollTop()) ->

  # Retrieving a set of menu items marked as current by Skrollr
  $active_menu_items = $menu_items.filter('.skrollable-between')

  # If we're at the top, return null
  if curTop < $window.height() / 2
    $menu_items.first()

    # If there's only one item in the set of current items, use it
  else if $active_menu_items.length is 1
    $active_menu_items

    # If there's more than one item, use the second item
  else if $active_menu_items.length > 1
    $active_menu_items.eq(1)

    # If we're not at the top and no items are active, return the last menu item
  else
    $menu_items.last()


current_href = ($current_menu_item = current_menu_item()) ->
  $current_menu_item.find('a').attr('href') or '#'


# Saving current menu item for 
original_menu_item = window.$menu_items.find("a[href='#{window.location.hash}']").get(0)

# Initializing Skrollr
window.skrollr_instance = skrollr.init

# Do not add extra space
  forceHeight: false

# Running a routine on scroll
  render: $.debounceLast 50, (skrollr_data) ->

    # Selecting which menu item to use
    $current_menu_item = current_menu_item(skrollr_data.curTop)

    if $current_menu_item.get(0) is $menu_items.get(0)
      $menu_button_prev.addClass disabled_classname
      $menu_button_next.removeClass disabled_classname

    else if $current_menu_item.get(0) is $menu_items.get($menu_items.length - 1)
      $menu_button_prev.removeClass disabled_classname
      $menu_button_next.addClass disabled_classname

    else
      $menu_button_prev.removeClass disabled_classname
      $menu_button_next.removeClass disabled_classname


    #     # Removing the "current" classname from all menu itmes
    #     $menu_items.removeClass current_classname

    #     # Applying the "current" classname to currnet menu item
    #     $current_menu_item.addClass current_classname

    # If current item is the first item, clear the anchor from the URL
    if $current_menu_item.get(0) is $menu_items.get(0)
      history?.pushState? null, null, window.location.pathname + window.location.search

    else
      # Retrieve the anchor from the menu item
      href = current_href $current_menu_item

      # Update the url with the anchor
      history?.pushState? null, null, href

# Initialize Skrollr menu
skrollr.menu.init window.skrollr_instance

# Scrolling to the position of original hash
if original_menu_item
  $.delay 1000, ->
    skrollr.menu.click original_menu_item


# Reacting to previous section button
$menu_button_prev.click (event) ->

  # Do not do default click action
  event.preventDefault()

  current_index = current_menu_item().index()

  # Do not do anything unless current item is not the first one
  if current_index > 0

    $target_menu_item = $menu_items.eq current_index - 1
    skrollr.menu.click $target_menu_item.find('a').get(0)


# Reacting to next section button
$menu_button_next.click (event) ->

  # Do not do default click action
  event.preventDefault()

  current_index = current_menu_item().index()

  # Do not do anything unless current item is not the last one
  if current_index < $menu_items.length - 1

    $target_menu_item = $menu_items.eq current_index + 1
    skrollr.menu.click $target_menu_item.find('a').get(0)





