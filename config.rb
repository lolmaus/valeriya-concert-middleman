# Change Compass configuration
compass_config do |config|
  config.output_style = (environment == :development) ? :nested : :compressed
  #config.sass_options = (environment == :development) ? {debug_info: true} : {}
end

# Compass deps
require 'singularitygs'

# Custom Sass functions
module Sass::Script::Functions
  def foo
    return Sass::Script::Bool.new(true)
  end
end


assets_path = '!assets/'
set :css_dir,       File.join(assets_path, 'stylesheets')
set :js_dir,        File.join(assets_path, 'javascripts')
set :images_dir,    File.join(assets_path, 'images')
set :fonts_dir,     File.join(assets_path, 'fonts')
set :partials_dir,  File.join(assets_path, 'partials')
set :layouts_dir ,  File.join(assets_path, '_layouts')

# Bitbucket
# set :build_dir, 'build/valeriya-concert-middleman'

activate :relative_assets
set :relative_links, true
activate :directory_indexes

set :layout, :skeleton



set :file_watcher_ignore,[
  /^bin\//,
  /^\.bundle\//,
  /^vendor\//,
  /^\.sass-cache\//,
  /^\.cache\//,
  /^\.git\//,
  /^\.gitignore$/,
  /\.DS_Store/,
  /^\.rbenv-.*$/,
  /^\.idea\//,
  /^Gemfile$/,
  /^Gemfile\.lock$/,
  /^Gruntfile\.js$/,
  /^package\.json$/,
  /^[a-zA-Z0-9_-]+\.iml$/,
  /~$/,
  /(^|\/)\.?#/,
  /^tmp\//,
  /^build\//,
  /^\.bowerrc\//,
  /^bower_components\//,
  /^node_modules\//
]

activate :blog do |blog|
  blog.name = 'sections'
  blog.sources = "sections/{year}-{month}-{day}-{title}.html"
  blog.permalink = "sections/{title}.html"
end


# Dev-specific configuration
configure :development do
  set :debug_assets, true

  # Reload the browser automatically whenever files change
  #activate :livereload, :host => "10.10.12.1"
  activate :livereload, :host => "127.0.0.1"
end



# Build-specific configuration
configure :build do
  # For example, change the Compass output style for deployment
  activate :minify_css

  # Minify Javascript on build
  activate :minify_javascript

  # Enable cache buster
  activate :asset_hash
end
