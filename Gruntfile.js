module.exports = function(grunt) {
  grunt.initConfig({


    modernizr: {

      // [REQUIRED] Path to the build you're using for development.
      "devFile" : "bower_components/modernizr/modernizr.js",

      // [REQUIRED] Path to save out the built file.
      "outputFile" : "source/_assets/javascripts/bower/modernizr-custom.js",

      // Based on default settings on http://modernizr.com/download/
      "extra" : {
        "shiv" : false,
        "printshiv" : false,
        "load" : true,
        "mq" : false,
        "cssclasses" : true
      },

      // Based on default settings on http://modernizr.com/download/
      "extensibility" : {
        "addtest" : false,
        "prefixed" : false,
        "teststyles" : false,
        "testprops" : false,
        "testallprops" : false,
        "hasevents" : false,
        "prefixes" : true,
        "domprefixes" : false
      },

      // By default, source is uglified before saving
      "uglify" : false,

      // Define any tests you want to implicitly include.
      "tests" : [
        "css-calc"
      ],

      // By default, this task will crawl your project for references to Modernizr tests.
      // Set to false to disable.
      "parseFiles" : false,

      // When parseFiles = true, this task will crawl all *.js, *.css, *.scss files, except files that are in node_modules/.
      // You can override this by defining a "files" array below.
      // "files" : [],

      // When parseFiles = true, matchCommunityTests = true will attempt to
      // match user-contributed tests.
      "matchCommunityTests" : true,

      // Have custom Modernizr tests? Add paths to their location here.
      "customTests" : []
    },


    shell: {                                // Task

        generateFontelloIcons: {            // Target
            options: {                      // Options
                stdout: true,
                stderr: true
            },
            command: "node_modules/.bin/fontello-svg -c fontello.json -o 'source/_assets/images/fontello-icons' -f 'black:#000 | grey-dusty:#999 | grey-emperor:#555 | grey-mineshaft:#333 | white:#fff | red-guardsman:#b00'"
        },

        buildAndPublish: {
            command: [
              'echo "### Building ###"',
              'bundle exec middleman build --verbose',

              'echo "### Adding built files to git index ###"',
              'cd build/',
              'git add -A',

              'echo "### Commiting changes ###"',
              'git commit -m build',

              'echo "### Pushing the commit to the remote branch ###"',
              'git push origin gh-pages',
              'cd ..'
            ].join(' && '),
            options: {
              stdout: true,
              stderr: true
            }
        }

    },


    copy: {
      bowercomponents: {
        files: [
          {
            expand: true,
            flatten: true,
            src: [
              'bower_components/jquery/dist/jquery.js',
              'bower_components/jquery-ui/ui/jquery.ui.widget.js',
              'bower_components/skrollr/src/skrollr.js',
              'bower_components/skrollr-menu/src/skrollr.menu.js',
              'bower_components/jQuery-One-Page-Nav/jquery.nav.js',
              'bower_components/jquery.timer-tools/jquery.timer-tools.coffee'
            ],
            dest: 'source/_assets/javascripts/bower/',
            filter: 'isFile'
          }
        ]
      }
    },


    bower: {
      install: {
        options: {
          copy: false
        }
      }
    }


  });

  grunt.loadNpmTasks('grunt-modernizr');
  grunt.loadNpmTasks('grunt-shell');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-bower-task');

  grunt.registerTask('default', [
    'bower:install',
    'copy:bowercomponents',
    'modernizr'
  ]);

  grunt.registerTask('publish',[
    'shell:buildAndPublish'
  ]);

  grunt.registerTask('bow', [
    'bower:install',
    'copy:bowercomponents'
  ]);

  grunt.registerTask('fontello-svg', [
    'shell:generateFontelloIcons'
  ]);
};